package org.frkn.application.status;


import org.frkn.graduate_study.application_status.controller.ApplicationController;
import org.frkn.graduate_study.application_status.model.MetuApplication;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class TestMetuApplication {

    private static final Logger LOG = LoggerFactory.getLogger( TestMetuApplication.class );

    public static String email;
    public static String passwd;
    public static MetuApplication metuApplication;
    public static WebDriver driver;

    @BeforeClass
    public static void init() throws IOException {
        Properties prop = new Properties();
        String propFileName = "secret-credentials.properties";

        InputStream inputStream = TestMetuApplication.class.getClassLoader().getResourceAsStream( propFileName );

        if ( inputStream != null ) {
            prop.load( inputStream );
        } else {
            throw new FileNotFoundException( "property file '" + propFileName + "' not found in the classpath" );
        }
        // get the property value and print it out
        email = prop.getProperty( "email" );
        passwd = prop.getProperty( "passwd" );
        LOG.debug( "email is set to: " + email + ", passwd is set to: " + passwd );
        MetuApplication.setCurrentDriver( MetuApplication.getWebDriver() );
    }


    @Test
    public void testSuccessfulApplicationStatus() {
        String[] credentials = new String[2];
        credentials[0] = email;
        credentials[1] = passwd;
        ApplicationController.main( credentials );
    }


    @Test( expected = NoSuchElementException.class )
    public void testInvalidLoginPage() {
        metuApplication = new MetuApplication( email, passwd );
        metuApplication.LOGIN_PAGE_URL = "http://www.google.com";
        metuApplication.getApplicationStatus();
    }


    @Test( expected = IllegalArgumentException.class )
    public void testMissingCredentials() {
        String[] credentials = new String[1];
        credentials[0] = email;
        ApplicationController.main( credentials );
    }

    @Test
    public void testWrongCredentials() {
        String wrongMail = "asdasd@invalid.email";
        String wrongPasswd = "invalid_pass";
        metuApplication = new MetuApplication( wrongMail, wrongPasswd );
        Assert.assertEquals( metuApplication.getApplicationStatus(), MetuApplication.ApplicationStatus.UNKNOWN );
    }


    @AfterClass
    public static void onClose() {
        if ( driver != null )
            driver.quit();
        if ( metuApplication != null && metuApplication.getCurrentDriver() != null ) {
            metuApplication.getCurrentDriver().quit();
        }
    }
}