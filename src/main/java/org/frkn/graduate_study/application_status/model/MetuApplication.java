package org.frkn.graduate_study.application_status.model;


import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.FailedLoginException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MetuApplication {

    private static final Logger LOG = LoggerFactory.getLogger( MetuApplication.class );

    private static WebDriver currentDriver;
    private static String email;
    private static String passwd;
    public String LOGIN_PAGE_URL = "https://oibs2.metu.edu.tr/Ms_Phd_Applications/";
    int tryCount = 0;


    public MetuApplication( String email, String passwd ) {
        this.email = email;
        this.passwd = passwd;
    }

    public static WebDriver getWebDriver() {
        if ( currentDriver == null || currentDriver.toString().contains( "null" ) )
            return new FirefoxDriver();
        else {
            try {
                currentDriver.getCurrentUrl();
            } catch ( Exception e ) {
                currentDriver.quit();
                return new FirefoxDriver();
            }
        }
        return currentDriver;
    }


    public ApplicationStatus getApplicationStatus() {
        currentDriver = getWebDriver();
        boolean urlReachable = false;
        try {
            tryCount++;
            urlReachable = isURLReachable( LOGIN_PAGE_URL );
            if ( urlReachable )
                loginPage();
        } catch ( FailedLoginException e ) {
            return ApplicationStatus.UNKNOWN;
        } catch ( IllegalStateException e2 ) {
            return ApplicationStatus.UNKNOWN;
        } catch ( ElementNotVisibleException e3 ) {
            if ( tryCount < 2 )
                getApplicationStatus();
        }
        if ( ! urlReachable )
            return ApplicationStatus.UNKNOWN;
        ApplicationStatus status = parseApplicationStatus();
        LOG.info( "APPLICATION STATUS: " + status );
        LOG.debug( String.format( "Page source: %s", currentDriver.getPageSource() ) );
        return status;
    }

    private boolean isURLReachable( String url ) {
        boolean isReachable = false;
        URL urlLink;
        try {
            urlLink = new URL( url );
            urlLink.openConnection();
            isReachable = true;
        } catch ( MalformedURLException e ) {
            e.printStackTrace();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        finally {
            return isReachable;
        }


    }


    private ApplicationStatus parseApplicationStatus() {
        ApplicationStatus status = ApplicationStatus.UNKNOWN;
        WebElement msapplist = currentDriver.findElement( By.id( "msapplist" ) );
        LOG.debug( "application list component:" + msapplist.toString() );
        WebElement table = msapplist.findElement( By.tagName( "table" ) );
        List<WebElement> tableRows = table.findElements( By.tagName( "tr" ) );
        List<String> rowTexts = new ArrayList<String>();
        WebElement row;
        for ( int i = 0; i < 100; i++ ) {
            try {
                row = tableRows.get( i );
            } catch ( Exception e ) {
                LOG.debug( "It seems there are " + i + " rows in the table..." );
                break;
            }
            if ( row == null ) {
                LOG.error( "Could not find any row in the table!" );
                return status;
            }
            String text = row.getText();
            rowTexts.add( text );
            LOG.debug( (i + 1) + "th row text: " + text );
        }

        if ( rowTexts.toString().contains( ApplicationStatus.REJECTED.getStatusEvaluationCriteria() ) ) {
            return ApplicationStatus.REJECTED;
        } else if ( rowTexts.toString().contains( ApplicationStatus.FREEZE.getStatusEvaluationCriteria() ) ) {
            return ApplicationStatus.FREEZE;
        } else if ( rowTexts.toString().contains( ApplicationStatus.IN_EVALUATION.getStatusEvaluationCriteria() ) ) {
            return ApplicationStatus.IN_EVALUATION;
        } else if ( rowTexts.toString().contains( ApplicationStatus.ACCEPTED.getStatusEvaluationCriteria() ) ) {
            return ApplicationStatus.ACCEPTED;
        }

        return status;
    }


    private void loginPage() throws FailedLoginException {
        // Open the page
        currentDriver.get( LOGIN_PAGE_URL );

        closePopups();

        // Find the email text input element by its name
        WebElement emailTextBox = currentDriver.findElement( By.name( "text_usercode" ) );
        emailTextBox.sendKeys( email );

        // Find the password input element by its name
        WebElement passwordTextBox = currentDriver.findElement( By.name( "text_password" ) );
        passwordTextBox.sendKeys( passwd );

        // Click the login button
        WebElement loginButton = currentDriver.findElement( By.name( "submit_auth" ) );
        loginButton.click();

        // Check that we're on the right page.
        if ( !currentDriver.getCurrentUrl().contains( "package" ) ) {
            if ( currentDriver.getPageSource().contains( "Your usercode is incorrect." )
                    || currentDriver.getPageSource().contains( "Your email information does not exist in the system" ) ) {
                throw new FailedLoginException( "Could not login with the provided credentials!" );
            } else {
                // TODO parse the error page and return more relevant response message
                throw new IllegalStateException( "This is not the personal logged in page("
                        + currentDriver.getCurrentUrl() + ")" );
            }
        }

        LOG.debug( "Page url is: " + currentDriver.getCurrentUrl() );
    }


    private void closePopups() {
        // Check if there are popups on the browser
        WebDriverWait wait = new WebDriverWait( currentDriver, 5 );
        WebElement modalclosebutton = null;

        try {
            modalclosebutton = wait.until( ExpectedConditions.presenceOfElementLocated( By.id( "modalclosebutton" ) ) );
            LOG.debug( "modalclosebutton is present, continuing on..." );
        } catch ( Exception e ) {
            LOG.info( "No popup found, continuing on..." );
        }

        // Return if the popups do not exist
        if ( modalclosebutton == null || !modalclosebutton.isDisplayed() ) {
            return;
        }

        // Close popups
        modalclosebutton.click();

        WebElement endTourButton;
        try {
            endTourButton = wait.until( ExpectedConditions.presenceOfElementLocated(
                    By.xpath( "//button[contains(.,'End tour')]" ) ) );
            LOG.debug( "End tour button is present, continuing on..." );
            endTourButton.click();
        } catch ( Exception e ) {
            LOG.info( "No popup found, continuing on..." );
        }
    }


    public static WebDriver getCurrentDriver() {
        return currentDriver;
    }

    public static void setCurrentDriver( WebDriver currentDriver ) {
        MetuApplication.currentDriver = currentDriver;
    }


    public enum ApplicationStatus {
        IN_EVALUATION( "IN EVALUATION" ),
        ACCEPTED( "ACCEPTED" ),
        REJECTED( "NOT ACCEPTED" ),
        FREEZE( "FREEZE" ),
        UNKNOWN( "UNKNOWN" );

        private final String statusEvaluationCriteria;

        ApplicationStatus( final String statusEvaluationCriteria ) {
            this.statusEvaluationCriteria = statusEvaluationCriteria;
        }

        public String getStatusEvaluationCriteria() {
            return statusEvaluationCriteria;
        }
    }
}
