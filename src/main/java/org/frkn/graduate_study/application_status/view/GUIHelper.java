package org.frkn.graduate_study.application_status.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUIHelper {

    private JFrame mainWindow;
    private JLabel emailLabel;
    private JLabel passwordLabel;
    private JLabel statusLabel;
    private JTextField emailArea;
    private JTextField passwordArea;
    private JButton submitButton;
    private JLabel applicationStatusLabel;
    private JScrollPane applicationStatusScrollPane;
    private JPanel credentialsPanel;

    public GUIHelper() {
        mainWindow = new JFrame( "Application Status Checker" );
        SpringLayout springLayout = new SpringLayout();
        credentialsPanel = new JPanel( springLayout );
        emailArea = new JTextField();
        emailLabel = new JLabel( "Email:" );
        passwordLabel = new JLabel( "Password:" );
        statusLabel = new JLabel( "Status:" );
        passwordArea = new JPasswordField();
        submitButton = new JButton( "Submit" );
        applicationStatusLabel = new JLabel();
        applicationStatusScrollPane = new JScrollPane( applicationStatusLabel );

        credentialsPanel.add( emailLabel );
        credentialsPanel.add( emailArea );
        credentialsPanel.add( passwordLabel );
        credentialsPanel.add( passwordArea );
        credentialsPanel.add( submitButton );
        credentialsPanel.add( statusLabel );
        credentialsPanel.add( applicationStatusScrollPane );

        // Arrange UP sides fields
        springLayout.putConstraint( SpringLayout.NORTH, emailLabel, 7, SpringLayout.NORTH, credentialsPanel );
        springLayout.putConstraint( SpringLayout.NORTH, emailArea, 5, SpringLayout.NORTH, credentialsPanel );
        springLayout.putConstraint( SpringLayout.NORTH, passwordLabel, 7, SpringLayout.SOUTH, emailArea );
        springLayout.putConstraint( SpringLayout.NORTH, passwordArea, 5, SpringLayout.SOUTH, emailArea );
        springLayout.putConstraint( SpringLayout.NORTH, submitButton, 5, SpringLayout.SOUTH, passwordArea );
        springLayout.putConstraint( SpringLayout.NORTH, statusLabel, 5, SpringLayout.SOUTH, submitButton );
        springLayout.putConstraint( SpringLayout.NORTH, applicationStatusScrollPane, 5, SpringLayout.SOUTH, submitButton );
        springLayout.putConstraint( SpringLayout.SOUTH, applicationStatusScrollPane, -5, SpringLayout.SOUTH, credentialsPanel );

        // Arrange LEFT AND RIGHT sides fields
        springLayout.putConstraint( SpringLayout.WEST, emailLabel, 5, SpringLayout.WEST, credentialsPanel );
        springLayout.putConstraint( SpringLayout.WEST, emailArea, 5, SpringLayout.EAST, passwordLabel );
        springLayout.putConstraint( SpringLayout.EAST, emailArea, -5, SpringLayout.EAST, credentialsPanel );
        springLayout.putConstraint( SpringLayout.WEST, passwordLabel, 5, SpringLayout.WEST, credentialsPanel );
        springLayout.putConstraint( SpringLayout.WEST, passwordArea, 5, SpringLayout.EAST, passwordLabel );
        springLayout.putConstraint( SpringLayout.EAST, passwordArea, -5, SpringLayout.EAST, credentialsPanel );
        springLayout.putConstraint( SpringLayout.WEST, statusLabel, 5, SpringLayout.WEST, credentialsPanel );
        springLayout.putConstraint( SpringLayout.WEST, applicationStatusScrollPane, 5, SpringLayout.EAST, passwordLabel );
        springLayout.putConstraint( SpringLayout.EAST, applicationStatusScrollPane, -5, SpringLayout.EAST, credentialsPanel );

        mainWindow.add( credentialsPanel );
        mainWindow.getContentPane().setPreferredSize( new Dimension( 400, 150 ) );
        mainWindow.setLocationRelativeTo( null );
        mainWindow.pack();
        mainWindow.setVisible( true );
        mainWindow.setAlwaysOnTop( true );
    }


    public void addSubmitButtonActionListener( ActionListener listener ) {
        submitButton.addActionListener( listener );
    }


    public JTextField getEmailArea() {
        return emailArea;
    }


    public JTextField getPasswordArea() {
        return passwordArea;
    }


    public JButton getSubmitButton() {
        return submitButton;
    }


    public JLabel getApplicationStatusLabel() {
        return applicationStatusLabel;
    }

}
