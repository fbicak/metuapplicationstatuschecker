package org.frkn.graduate_study.application_status.controller;


import org.frkn.graduate_study.application_status.model.MetuApplication;
import org.frkn.graduate_study.application_status.view.GUIHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ApplicationController {

    private static final Logger LOG = LoggerFactory.getLogger( ApplicationController.class );

    private MetuApplication metuApplication;
    private GUIHelper mainView;


    public ApplicationController() {
        mainView = new GUIHelper();
        mainView.addSubmitButtonActionListener( getSubmitButtonActionListener() );
    }


    public ApplicationController( String email, String passwd ) {
        metuApplication = new MetuApplication( email, passwd );
    }


    public static void main( String[] args ) {
        ApplicationController applicationController;
        if ( args.length > 0 && args.length != 2 ) {
            throw new IllegalArgumentException( "There should be exactly 2 arguments email " +
                    "and password respectively! Exiting..." );
        } else if ( args.length == 0 ) {
            applicationController = new ApplicationController();
            applicationController.getMainView();
        } else {
            String email = args[0];
            String passwd = args[1];

            applicationController = new ApplicationController( email, passwd );
            applicationController.metuApplication.getApplicationStatus();
            MetuApplication.getCurrentDriver().quit();
        }
    }


    GUIHelper getMainView() {
        if ( mainView == null )
            return new GUIHelper();
        return mainView;
    }


    public ActionListener getSubmitButtonActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                String email = mainView.getEmailArea().getText();
                String passwd = mainView.getPasswordArea().getText();

                if ( email != null && passwd != null && !email.equals( "" ) && !passwd.equals( "" ) ) {
                    try {
                        mainView.getApplicationStatusLabel().setVisible( false );
                        mainView.getSubmitButton().setEnabled( false );
                        metuApplication = new MetuApplication( email, passwd );
                        MetuApplication.ApplicationStatus applicationStatus = metuApplication.getApplicationStatus();
                        mainView.getApplicationStatusLabel().setText( applicationStatus.getStatusEvaluationCriteria() );
                        LOG.debug( "Application status: " + applicationStatus.getStatusEvaluationCriteria() );
                        MetuApplication.getCurrentDriver().quit();
                    } catch ( Exception e1 ) {
                        mainView.getApplicationStatusLabel().setText( "Error occurred, please check logs:\n" + e1 );
                    } finally {
                        mainView.getSubmitButton().setEnabled( true );
                        mainView.getApplicationStatusLabel().setVisible( true );
                    }
                }
            }
        };
    }
}
