# What this project is for?
I recently have made an master application to METU at Computer Engineering department and wanted to automate the process of checking the status of my application.

# I also want to run tests but how?
Since the application status page requires some valid credentials, you need to enter your credentials as follows:
file location:
  src/test/resources/secret-credentials.properties
file content:
  email=registered@mail-address.here
  passwd=valid-password-here

# What do I need to run this project?
This project relies on Selenium to submit login credentials and get the results. Currently used webdriver is FirefoxDriver so you need to have Firefox installed on the machine that you run this application.

# How to run the project?
If you have the requirements described above, you should build the project(mvn clean package -DskipTests) and then you can run any of the .jar files(suggested one is *-jar-with-dependencies.jar) under target directory as follows:
If you want to see a graphical user interface, then just run the jar as follows:
java -jar target/jar-file-name.jar
Or if you want to provide the email and password information from command line, execute the following command:
java -jar target/jar-file-name.jar "mail-address" "password"
